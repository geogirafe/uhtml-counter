# µhtml + Vite with html templated files

This is a simple app with two components written in typescript using µhtml library, bundled with Vite and sharing data through a simple statemanager using on-change library.

# Getting started

```
npm i
npm start
```

# File structure

```
📦root
 ┣ 📂buildtools           // contains two scripts for vite allowing html file templating
 ┗ 📂src
   ┣ 📂components         // contains your components
   ┣ 📂state
   ┃ ┣ 📜state.ts         // shared data accross the app
   ┃ ┗ 📜statemanager.ts  // handles changes made in the state
   ┣ 📜index.html         // where your componenents are used
   ┗ 📜main.ts            // where your componenents are imported and defined
```
