import { render } from 'uhtml';
import { Hole } from 'uhtml/keyed';
import StateManager from '../../state/statemanager';

class MyBillboard extends HTMLElement {
  template?: () => Hole;
  stateManager: StateManager;
  templateUrl = './template.html';
  styleUrl = './style.css';

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.stateManager = StateManager.getInstance();
  }

  get state() {
    return this.stateManager.state;
  }

  get count() {
    return this.state.counter;
  }

  registerEvents() {
    this.stateManager.subscribe('counter', (_oldValue, _newValue) => {
      this.update();
    });
  }

  connectedCallback() {
    this.update();
    this.registerEvents();
  }

  update() {
    render(this.shadowRoot, this.template!);
  }
}

export default MyBillboard;
