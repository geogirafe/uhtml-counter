import './style.css';
import MyCounter from './components/counter/counter';
import MyBillboard from './components/billboard/billboard';

customElements.define('my-counter', MyCounter);
customElements.define('my-billboard', MyBillboard);
